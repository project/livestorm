CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The Livestorm module provides a webhook intergration with [Livestorm.co](https://livestorm.co/).

To know about Livestorm Webhooks Integration, visit [Livestrom support documents](https://support.livestorm.co/article/119-webhooks).

For a full description of the project visit the project page:
http://drupal.org/project/livestorm

To submit bug reports and feature suggestions, or to track changes:
http://drupal.org/project/issues/livestorm


REQUIREMENTS
------------

No special requirements

CONFIGURATION
-------------

 * Configure Livestorm settings in Administration » Configuration » Web services » Livestorm settings:

   - Webhook Token: a security token that used to verify valid routing. This token will be set when create Livestorm webhook by this format:
   _https://domain.name/livestorm/{event_type}/{token}_

   - Log incoming webhooks: checkbox to allow/disallow the webhook.

CUSTOMIZATION
-------------

No Customization.


TROUBLESHOOTING
-------------

Updating.

FAQ
---
Updating.

MAINTAINERS
-----------

Current maintainers:
 * Rupert Jabelman (rupertj) - https://www.drupal.org/u/rupertj
 * Joeri Poesen (jpoesen) - https://www.drupal.org/u/jpoesen
 * Thi Dang (mrddthi) - https://www.drupal.org/user/3626735
