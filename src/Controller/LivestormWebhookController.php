<?php

namespace Drupal\livestorm\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\livestorm\Event\LivestormNewRegistrantEvent;
use Drupal\livestorm\Event\LivestormWebinarStartEvent;
use Drupal\livestorm\Event\LivestormWebinarEndEvent;
use Drupal\livestorm\Event\LivestormWebinarPublishedEvent;

/**
 * Class LivestormWebhookController.
 *
 * Provides the route functionality for livestorm.webhook route.
 */
class LivestormWebhookController extends ControllerBase {

  /**
   * The ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The RequestStack definition.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The EventDispatcherInterface definition.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public function __construct(LoggerInterface $logger, ConfigFactoryInterface $config_factory, EventDispatcherInterface $event_dispatcher) {
    $this->logger = $logger;
    $this->config = $config_factory->get('livestorm.settings');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.channel.livestorm'),
      $container->get('config.factory'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Captures the incoming webhook request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Http Request.
   * @param string $event_type
   *   Livestorm event type.
   * @param string $token
   *   Livestorm given token.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Http Response.
   */
  public function handleIncomingWebhook(Request $request, $event_type, $token) {
    if (!$this->config->get('log_webhooks')
      || $this->config->get('webhook_token') !== $token
      || !in_array($event_type, [
        'new_registrant',
        'webinar_start',
        'webinar_end',
        'webinar_published',
      ])
    ) {
      return new Response(NULL, Response::HTTP_FORBIDDEN);
    }

    $input = $request->getContent();
    $decoded_input = Json::decode($input);

    if (!$decoded_input) {
      $this->logger->error('Invalid webhook event: @data', ['@data' => $input]);
      return new Response(NULL, Response::HTTP_FORBIDDEN);
    }

    $this->logger->info("Livestorm webhook received event:\n @event", ['@event' => (string) $event_type]);

    // Dispatch the webhook event base on event type.
    switch ($event_type) {
      case 'new_registrant':
        $e = new LivestormNewRegistrantEvent($decoded_input);
        $this->eventDispatcher->dispatch('livestorm.new_registrant', $e);
        break;

      case 'webinar_start':
        $e = new LivestormWebinarStartEvent($decoded_input);
        $this->eventDispatcher->dispatch('livestorm.webinar_start', $e);
        break;

      case 'webinar_end':
        $e = new LivestormWebinarEndEvent($decoded_input);
        $this->eventDispatcher->dispatch('livestorm.webinar_end', $e);
        break;

      default:
        $e = new LivestormWebinarPublishedEvent($decoded_input);
        $this->eventDispatcher->dispatch('livestorm.webinar_published', $e);
    }

    return new Response('Okay', Response::HTTP_OK);
  }

}
