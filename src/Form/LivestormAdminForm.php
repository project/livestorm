<?php

namespace Drupal\livestorm\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LivestormAdminForm.
 *
 * Contains admin form functionality for the Livestorm.
 */
class LivestormAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'livestorm_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'livestorm.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('livestorm.settings');

    $form['link'] = [
      '#markup' => $this->t('Livestorm links: <a href="@support" target="_blank">Livestorm Support</a> | <a href="@webhook" target="_blank">Webhooks Integration</a><br /><br />', [
        '@support' => Url::fromUri('https://support.livestorm.co/', ['attributes' => ['target' => '_blank']])->toString(),
        '@webhook' => Url::fromUri('https://support.livestorm.co/article/119-webhooks', ['attributes' => ['target' => '_blank']])->toString(),
      ]),
    ];

    $form['webhook_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Webhook Token'),
      '#default_value' => $config->get('webhook_token'),
    ];

    $form['log_webhooks'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log incoming webhooks'),
      '#default_value' => $config->get('log_webhooks'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('livestorm.settings')
      ->set('log_webhooks', $form_state->getValue('log_webhooks'))
      ->set('webhook_token', $form_state->getValue('webhook_token'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
