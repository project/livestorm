<?php

namespace Drupal\livestorm\Event;

/**
 * Class LivestormWebinarPublishedEvent.
 *
 * @package Drupal\livestorm\Event
 */
class LivestormWebinarPublishedEvent extends LivestormWebhookEvent {

  /**
   * {@inheritdoc}
   */
  public function __construct($data) {
    parent::__construct('webinar_published', $data);
  }

}
