<?php

namespace Drupal\livestorm\Event;

/**
 * Class LivestormWebinarEndEvent.
 *
 * @package Drupal\livestorm\Event
 */
class LivestormWebinarEndEvent extends LivestormWebhookEvent {

  /**
   * {@inheritdoc}
   */
  public function __construct($data) {
    parent::__construct('webinar_end', $data);
  }

}
