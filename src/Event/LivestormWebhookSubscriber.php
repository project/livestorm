<?php

namespace Drupal\livestorm\Event;

use Drupal\Component\Serialization\Json;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class LivestormWebhookSubscriber.
 *
 * Provides the event subscribers for 4 type of Livestorm webhook event.
 */
class LivestormWebhookSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      'livestorm.new_registrant' => 'logMessages',
      'livestorm.webinar_start' => 'logMessages',
      'livestorm.webinar_end' => 'logMessages',
      'livestorm.webinar_published' => 'logMessages',
    ];
  }

  /**
   * Write a webhook log message.
   *
   * @param \Drupal\livestorm\Event\LivestormWebhookEvent $event
   *   Livestorm event.
   */
  public function logMessages(LivestormWebhookEvent $event) {
    \Drupal::logger('livestorm')
      ->notice('Processed webhook: @name<br /><br />Data: @data', [
        '@name' => $event->type,
        '@data' => Json::encode($event->data),
      ]);
  }

}
