<?php

namespace Drupal\livestorm\Event;

/**
 * Class LivestormNewRegistrantEvent.
 *
 * @package Drupal\livestorm\Event
 */
class LivestormNewRegistrantEvent extends LivestormWebhookEvent {

  /**
   * {@inheritdoc}
   */
  public function __construct($data) {
    parent::__construct('new_registrant', $data);
  }

}
