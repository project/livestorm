<?php

namespace Drupal\livestorm\Event;

/**
 * Class LivestormWebinarStartEvent.
 *
 * @package Drupal\livestorm\Event
 */
class LivestormWebinarStartEvent extends LivestormWebhookEvent {

  /**
   * {@inheritdoc}
   */
  public function __construct($data) {
    parent::__construct('webinar_published', $data);
  }

}
