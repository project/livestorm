<?php

namespace Drupal\livestorm\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class LivestormWebhookEvent.
 *
 * Provides the Livestorm Webhook Event.
 */
class LivestormWebhookEvent extends Event {

  /**
   * Livestorm Webhook Event type.
   *
   * @var string
   */
  public $type;

  /**
   * Livestorm Event Data.
   *
   * @var array
   */
  public $data;

  /**
   * Sets the default values for the event.
   *
   * @param string $type
   *   Webhook event type.
   * @param array $data
   *   Webhook event data.
   */
  public function __construct($type, array $data) {
    $this->type = $type;
    $this->data = $data;
  }

}
